#Prueba tecnica de Merqueo


##Requeriminetos tecnicos:

1. Android Studio 3.4.2
2. Minima version Android 23
3. Version objetivo Android 29
4. Kotlin v1.3.41
5. Bibliotecas utilizadas:
   * Navigation v2.0.0
   * Moshi v1.8.0
   * Retrofit v2.6.0
   * Koin v2.0.1
   * RX Kotlin v2.3.0
   * RX Android v2.1.1
   * Glide v4.9.0
   * Room v2.1.0 


## Propuesta: 

El proyecto se diseño bajo la arquitectura MVVM, usando también el patron Repository, consta de las siguientes capas:

**Capa Model:**
Es la encarga de obtener los datos haciendo uso del repositorio, la fuente datos puede ser desde un API remota o un Base de Datos Local.

**Capa View:**
Es la encargada de mostrar los datos al usuario, haciendo uso de Navigation para navegar dentro de la aplicación.                      

**Capa ViewModel:**
Es la encargada de conectar la vista con el modelo, haciendo uso de LiveData para actualizar los datos en la vista y de RX para solicitar los datos al modelo de manera asíncrona.


## Screenshots: 

![picture](screenshots/listMovies.png)
![picture](screenshots/detailMovie1.png)
![picture](screenshots/detailMovie2.png)
![picture](screenshots/listMoviesCart.png)
![picture](screenshots/cartEmpty.png)

