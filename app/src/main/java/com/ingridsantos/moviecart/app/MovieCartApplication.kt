package com.ingridsantos.moviecart.app

import android.app.Application
import com.ingridsantos.moviecart.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MovieCartApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MovieCartApplication)
            androidLogger()
            modules(listOf(applicationModule))
        }

    }
}