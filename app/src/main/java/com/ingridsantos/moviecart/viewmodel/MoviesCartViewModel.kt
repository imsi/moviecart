package com.ingridsantos.moviecart.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ingridsantos.moviecart.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class MoviesCartViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val moviesCartMutableLiveData: MutableLiveData<UIState> = MutableLiveData()
    private val clearCartMutableLiveData: MutableLiveData<UIState> = MutableLiveData()

    private val subscriptions = CompositeDisposable()

    fun getMoviesCartLiveData(): LiveData<UIState> = moviesCartMutableLiveData
    fun getClearCartLiveData(): LiveData<UIState> = clearCartMutableLiveData

    fun getMoviesInCart() {
        subscriptions.add(
                movieRepository.getMoviesInCart()
                        .doOnSubscribe {
                            moviesCartMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    moviesCartMutableLiveData.postValue(UIState.Success(it))
                                },
                                onError = {
                                    moviesCartMutableLiveData.postValue(UIState.Error(it.message
                                            ?: "Ah ocurrido un error"))
                                },
                                onComplete = {

                                }
                        )
        )
    }

    fun clearCart() {
        subscriptions.add(
                movieRepository.clearCart()
                        .doOnSubscribe {
                            clearCartMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onComplete = {
                                    clearCartMutableLiveData.postValue(UIState.Success(true))
                                },
                                onError = {
                                    clearCartMutableLiveData.postValue(UIState.Error(it.message
                                            ?: "Ah ocurrido un error"))
                                }
                        )
        )

    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }
}