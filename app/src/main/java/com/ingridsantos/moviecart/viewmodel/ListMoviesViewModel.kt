package com.ingridsantos.moviecart.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ingridsantos.moviecart.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ListMoviesViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val moviesMutableLiveData: MutableLiveData<UIState> = MutableLiveData()
    private val movieInCartMutableLiveData: MutableLiveData<UIState> = MutableLiveData()

    private val subscriptions = CompositeDisposable()

    fun getMoviesLiveData(): LiveData<UIState> = moviesMutableLiveData
    fun getMovieInCartLiveData(): LiveData<UIState> = movieInCartMutableLiveData

    fun getMoviesRemote() {
        subscriptions.add(
                movieRepository.discoverMovies()
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onSuccess = {
                                    Log.i(TAG, "onSuccess")
                                },
                                onError = {
                                    Log.i(TAG, it.message ?: "Ah ocurrido un error")
                                }
                        )
        )

    }

    fun getMoviesLocal() {
        subscriptions.add(
                movieRepository.getMoviesLocal()
                        .doOnSubscribe {
                            moviesMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    moviesMutableLiveData.postValue(UIState.Success(it))
                                },
                                onError = {
                                    moviesMutableLiveData.postValue(UIState.Error(it.message
                                            ?: "Ah ocurrido un error"))
                                },
                                onComplete = {

                                }
                        )
        )
    }

    fun setMovieInCart(idMovie: Int, inCart: Boolean) {
        subscriptions.add(
                movieRepository.setMovieInCart(idMovie = idMovie, inCart = inCart)
                        .doOnSubscribe {
                            movieInCartMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(onComplete = {
                            movieInCartMutableLiveData.postValue(UIState.Success(true))
                        }, onError = {
                            movieInCartMutableLiveData.postValue(UIState.Error(it.message ?: "Ah ocurrido un error"))
                        })
        )
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }

    companion object {
        const val TAG = "ListMoviesViewModel"
    }
}