package com.ingridsantos.moviecart.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ingridsantos.moviecart.model.repository.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class DetailMovieViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val detailMovieMutableLiveData: MutableLiveData<UIState> = MutableLiveData()
    private val movieInCartMutableLiveData: MutableLiveData<UIState> = MutableLiveData()

    private val subscriptions = CompositeDisposable()

    fun detailMovieLiveData(): LiveData<UIState> = detailMovieMutableLiveData
    fun getMovieInCartLiveData(): LiveData<UIState> = movieInCartMutableLiveData

    fun detailMovie(id: Int) {
        subscriptions.add(
                movieRepository.getMovie(id)
                        .doOnSubscribe {
                            detailMovieMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onNext = {
                                    detailMovieMutableLiveData.postValue(UIState.Success(it))
                                },
                                onError = {
                                    detailMovieMutableLiveData.postValue(UIState.Error(it.message
                                            ?: "Ah ocurrido un error"))
                                },
                                onComplete = {

                                }
                        )
        )

    }

    fun setMovieInCart(idMovie: Int, inCart: Boolean) {
        subscriptions.add(
                movieRepository.setMovieInCart(idMovie = idMovie, inCart = inCart)
                        .doOnSubscribe {
                            movieInCartMutableLiveData.postValue(UIState.Loading)
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribeBy(
                                onComplete = {
                                    movieInCartMutableLiveData.postValue(UIState.Success(true))
                                },
                                onError = {
                                    movieInCartMutableLiveData.postValue(UIState.Error(it.message
                                            ?: "Ah ocurrido un error"))
                                }
                        )
        )
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.dispose()
    }

}