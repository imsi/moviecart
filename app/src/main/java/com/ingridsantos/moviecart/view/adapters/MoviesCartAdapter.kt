package com.ingridsantos.moviecart.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ingridsantos.moviecart.R
import com.ingridsantos.moviecart.model.entities.Movie
import kotlinx.android.synthetic.main.item_movie_cart.view.*

class MoviesCartAdapter(private val dataItems: MutableList<Movie> = mutableListOf()) : RecyclerView.Adapter<MoviesCartAdapter.ViewHolder>() {

    fun setData(data : MutableList<Movie>){
        dataItems.clear()
        dataItems.addAll(data)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_cart, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = dataItems[position]
        holder.bind(movie)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Movie) {
            itemView.titleMovieTextView.text = item.title
            setImageFromURL("https://image.tmdb.org/t/p/w500${item.posterPath}")
        }

        private fun setImageFromURL(url: String) {
            Glide.with(itemView.context)
                    .load(url)
                    .into(itemView.posterPathImageView)
        }
    }
}