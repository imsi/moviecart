package com.ingridsantos.moviecart.view.fragments


import android.app.AlertDialog
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.ingridsantos.moviecart.R
import com.ingridsantos.moviecart.model.entities.Movie
import com.ingridsantos.moviecart.view.adapters.ListMovieAdapter
import com.ingridsantos.moviecart.viewmodel.ListMoviesViewModel
import com.ingridsantos.moviecart.viewmodel.UIState
import kotlinx.android.synthetic.main.fragment_list_movies.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListMoviesFragment : Fragment() {

    private val listMoviesViewModel: ListMoviesViewModel by viewModel()

    private lateinit var listMovieAdapter: ListMovieAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_movies, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listMoviesViewModel.getMoviesLocal()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupHandlers()
        setupListeners()
    }

    override fun onResume() {
        super.onResume()
        if (checkNetwork()) {
            listMoviesViewModel.getMoviesRemote()
        }
    }

    private fun setupUI() {
        listMovieAdapter = ListMovieAdapter(clickDetailMovieClosure = { id ->
            val bundle = Bundle()
            bundle.putInt("idMovie", id)
            Navigation.findNavController(view!!).navigate(R.id.action_listMoviesFragment_to_detailMovieFragment, bundle)
        }, clickSetMovieInCartClosure = { id, inCart ->
            listMoviesViewModel.setMovieInCart(id, inCart)
        })
        moviesRecyclerView.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        moviesRecyclerView.adapter = listMovieAdapter
    }

    private fun setupHandlers() {
        listMoviesViewModel.getMoviesLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG, "Loading ...")
                }
                is UIState.Success<*> -> {
                    val data = status.data as MutableList<Movie>
                    listMovieAdapter.setData(data)
                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()

                }
            }
        })

        listMoviesViewModel.getMovieInCartLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG,"Loading ...")
                }
                is UIState.Success<*> -> {
                    Log.i(TAG,"Accion realizada")
                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()
                }
            }
        })
    }

    private fun checkNetwork(): Boolean {
        activity?.run {
            val connectivityManager = ContextCompat.getSystemService(this.applicationContext, ConnectivityManager::class.java)
            val network = connectivityManager?.activeNetwork

            network?.run {
                val networkCapabilities = connectivityManager.getNetworkCapabilities(this)
                return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || networkCapabilities.hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                )
            }
        }
        return false
    }

    private fun setupListeners(){
        moviesInCartButton.setOnClickListener {
            Navigation.findNavController(view!!).navigate(R.id.action_listMoviesFragment_to_moviesCartFragment)
        }
    }

    companion object {
        const val TAG = "ListMoviesFragment"
    }
}
