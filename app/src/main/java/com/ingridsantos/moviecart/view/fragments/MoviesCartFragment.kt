package com.ingridsantos.moviecart.view.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.ingridsantos.moviecart.R
import com.ingridsantos.moviecart.model.entities.Movie
import com.ingridsantos.moviecart.view.adapters.MoviesCartAdapter
import com.ingridsantos.moviecart.viewmodel.MoviesCartViewModel
import com.ingridsantos.moviecart.viewmodel.UIState
import kotlinx.android.synthetic.main.fragment_movies_cart.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MoviesCartFragment : Fragment() {

    private val moviesCartViewModel: MoviesCartViewModel by viewModel()

    private lateinit var moviesCartAdapter: MoviesCartAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        moviesCartViewModel.getMoviesInCart()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupHandlers()
        setupListeners()
    }

    private fun setupUI() {
        moviesCartAdapter = MoviesCartAdapter()
        moviesInCartRecyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        moviesInCartRecyclerView.adapter = moviesCartAdapter
    }

    private fun setupHandlers() {
        moviesCartViewModel.getMoviesCartLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG, "Loading ...")
                }
                is UIState.Success<*> -> {
                    val data = status.data as MutableList<Movie>
                    moviesCartAdapter.setData(data)

                    if (data.isEmpty()) {
                        moviesInCartRecyclerView.visibility = GONE
                        messageNoneTextView.visibility = VISIBLE
                        clearCartButton.isEnabled = false
                        clearCartButton.alpha = 0.5F
                    } else {
                        messageNoneTextView.visibility = GONE
                        moviesInCartRecyclerView.visibility = VISIBLE
                        clearCartButton.isEnabled = true
                        clearCartButton.alpha = 1F
                    }
                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()
                }
            }
        })

        moviesCartViewModel.getClearCartLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG, "Loading ...")
                }
                is UIState.Success<*> -> {
                    Log.i(TAG, "Cart deleted successfully")
                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()
                }
            }
        })
    }

    private fun setupListeners() {
        clearCartButton.setOnClickListener {
            moviesCartViewModel.clearCart()
        }
    }

    companion object {
        const val TAG = "MoviesCartFragment"
    }
}
