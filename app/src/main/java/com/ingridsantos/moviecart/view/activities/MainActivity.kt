package com.ingridsantos.moviecart.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ingridsantos.moviecart.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
