package com.ingridsantos.moviecart.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ingridsantos.moviecart.R
import com.ingridsantos.moviecart.model.entities.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

class ListMovieAdapter(private val dataItems: MutableList<Movie> = mutableListOf(), val clickDetailMovieClosure: (Int) -> Unit, val clickSetMovieInCartClosure: (Int, Boolean) -> Unit) :
        RecyclerView.Adapter<ListMovieAdapter.ViewHolder>() {

    fun setData(data: MutableList<Movie>) {
        dataItems.clear()
        dataItems.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = dataItems[position]
        holder.bind(movie)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Movie) {
            itemView.titleMovieTextView.text = item.title
            itemView.releaseDateTextView.text = item.releaseDate
            itemView.voteAverageTextView.text = item.voteAverage.toString()
            itemView.popularityTextView.text = item.popularity.toString()
            itemView.overviewTextView.text = item.overview
            setImageFromURL("https://image.tmdb.org/t/p/w500${item.posterPath}")

            if (item.inCart) {
                itemView.addMovie.visibility = GONE
                itemView.removeMovie.visibility = VISIBLE
            } else {
                itemView.removeMovie.visibility = GONE
                itemView.addMovie.visibility = VISIBLE
            }

            itemView.setOnClickListener {
                clickDetailMovieClosure.invoke(item.id)
            }

            itemView.addMovie.setOnClickListener {
                clickSetMovieInCartClosure.invoke(item.id, true)
            }

            itemView.removeMovie.setOnClickListener {
                clickSetMovieInCartClosure.invoke(item.id, false)
            }
        }

        private fun setImageFromURL(url: String) {
            Glide.with(itemView.context)
                    .load(url)
                    .into(itemView.posterPathImageView)
        }
    }
}