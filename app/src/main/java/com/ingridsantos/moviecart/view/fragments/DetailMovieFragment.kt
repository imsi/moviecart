package com.ingridsantos.moviecart.view.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.ingridsantos.moviecart.R
import com.ingridsantos.moviecart.model.entities.Movie
import com.ingridsantos.moviecart.viewmodel.DetailMovieViewModel
import com.ingridsantos.moviecart.viewmodel.UIState
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailMovieFragment : Fragment() {

    private val detailMovieViewModel: DetailMovieViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = arguments?.getInt("idMovie", 0) ?: 0
        detailMovieViewModel.detailMovie(id)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHandlers()
        setupListeners()
    }

    private fun setupHandlers() {
        detailMovieViewModel.detailMovieLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG, "Loading ...")
                }
                is UIState.Success<*> -> {
                    val data = status.data as Movie
                    titleMovieTextView.text = data.title
                    releaseDateTextView.text = data.releaseDate
                    popularityTextView.text = data.popularity.toString()
                    voteAverageTextView.text = data.voteAverage.toString()
                    overviewMovieTextView.text = data.overview

                    Glide.with(activity!!)
                            .load("https://image.tmdb.org/t/p/w500${data.backdropPath}")
                            .into(backdropPathImageView)

                    if (data.inCart) {
                        addMovie.visibility = GONE
                        removeMovie.visibility = VISIBLE
                    } else {
                        removeMovie.visibility = GONE
                        addMovie.visibility = VISIBLE
                    }

                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()
                }
            }
        })

        detailMovieViewModel.getMovieInCartLiveData().observe(this, Observer { status ->
            when (status) {
                is UIState.Loading -> {
                    Log.i(TAG, "Loading ...")
                }
                is UIState.Success<*> -> {
                    Log.i(TAG, "Accion realizada")
                }
                is UIState.Error -> {
                    AlertDialog.Builder(activity)
                            .setMessage(status.message)
                            .create()
                            .show()
                }
            }
        })
    }

    private fun setupListeners() {

        val id = arguments?.getInt("idMovie", 0) ?: 0

        addMovie.setOnClickListener {
            detailMovieViewModel.setMovieInCart(idMovie = id, inCart = true)
        }
        removeMovie.setOnClickListener {
            detailMovieViewModel.setMovieInCart(idMovie = id, inCart = false)
        }
    }

    companion object {
        const val TAG = "DetailMovieFragment"
    }
}
