package com.ingridsantos.moviecart.model.entities

import com.squareup.moshi.Json

data class Discover(

        @field:Json(name = "results")
        val results: List<Movie>

)