package com.ingridsantos.moviecart.model.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "movie")
data class Movie(

        @PrimaryKey
        @ColumnInfo(name = "id")
        @field:Json(name = "id")
        val id: Int,

        @ColumnInfo(name = "title")
        @field:Json(name = "title")
        val title: String,

        @ColumnInfo(name = "overview")
        @field:Json(name = "overview")
        val overview: String,

        @ColumnInfo(name = "releaseDate")
        @field:Json(name = "release_date")
        val releaseDate: String,

        @ColumnInfo(name = "posterPath")
        @field:Json(name = "poster_path")
        val posterPath: String,

        @ColumnInfo(name = "voteAverage")
        @field:Json(name = "vote_average")
        val voteAverage: Float,

        @ColumnInfo(name = "popularity")
        @field:Json(name = "popularity")
        val popularity: Float,

        @ColumnInfo(name = "backdropPath")
        @field:Json(name = "backdrop_path")
        val backdropPath: String,

        @field:Json(name = "inCart")
        val inCart: Boolean = false

)