package com.ingridsantos.moviecart.model.repository

import com.ingridsantos.moviecart.model.entities.Movie
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MovieRepository {

    fun discoverMovies(): Single<List<Movie>>

    fun insertMoviesLocal(movies: List<Movie>)

    fun getMoviesLocal(): Flowable<List<Movie>>

    fun getMovie(id: Int): Flowable<Movie>

    fun setMovieInCart(idMovie: Int, inCart: Boolean): Completable

    fun getMoviesInCart(): Flowable<List<Movie>>

    fun clearCart(): Completable
}