package com.ingridsantos.moviecart.model.repository.network

import com.ingridsantos.moviecart.model.entities.Discover
import io.reactivex.Single
import retrofit2.http.GET

interface MoviesAPI {
    @GET(value = "discover/movie?sort_by=popularity.desc")
    fun discoverMovies(): Single<Discover>
}