package com.ingridsantos.moviecart.model.repository

import android.annotation.SuppressLint
import android.util.Log
import com.ingridsantos.moviecart.model.entities.Movie
import com.ingridsantos.moviecart.model.repository.database.MovieDAO
import com.ingridsantos.moviecart.model.repository.network.MoviesAPI
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class MovieRepositoryImp(private val moviesAPI: MoviesAPI, private val movieDAO: MovieDAO) : MovieRepository {

    override fun discoverMovies(): Single<List<Movie>> {
        return moviesAPI.discoverMovies()
                .flatMap {
                    insertMoviesLocal(it.results)
                    Single.just(it.results)
                }
    }

    @SuppressLint("CheckResult")
    override fun insertMoviesLocal(movies: List<Movie>) {
        movieDAO.insertMovies(movies)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onComplete = {
                            Log.i(TAG, "onComplete")
                        },
                        onError = {
                            Log.i(TAG, it.message ?: "Ah ocurrido un error")
                        }
                )
    }

    override fun getMoviesLocal(): Flowable<List<Movie>> = movieDAO.getMovies()

    override fun getMovie(id: Int): Flowable<Movie> = movieDAO.getMovie(id)

    override fun setMovieInCart(idMovie: Int, inCart: Boolean): Completable = movieDAO.setMovieInCart(idMovie, inCart)

    override fun getMoviesInCart(): Flowable<List<Movie>> = movieDAO.getMoviesInCart()

    override fun clearCart(): Completable = movieDAO.clearCart()

    companion object {
        const val TAG = "MovieRepositoryImp"
    }
}