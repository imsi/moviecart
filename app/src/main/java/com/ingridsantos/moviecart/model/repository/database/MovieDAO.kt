package com.ingridsantos.moviecart.model.repository.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ingridsantos.moviecart.model.entities.Movie
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface MovieDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertMovies(movies: List<Movie>): Completable

    @Query("SELECT * FROM movie ORDER BY popularity DESC")
    fun getMovies(): Flowable<List<Movie>>

    @Query("SELECT * FROM movie WHERE id=:id")
    fun getMovie(id: Int): Flowable<Movie>

    @Query("UPDATE movie SET inCart=:inCart WHERE id=:idMovie")
    fun setMovieInCart(idMovie: Int, inCart: Boolean): Completable

    @Query("SELECT * FROM movie WHERE inCart=1")
    fun getMoviesInCart():Flowable<List<Movie>>

    @Query("UPDATE movie SET inCart=0")
    fun clearCart() : Completable
}