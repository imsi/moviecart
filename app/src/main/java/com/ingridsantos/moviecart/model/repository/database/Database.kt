package com.ingridsantos.moviecart.model.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ingridsantos.moviecart.model.entities.Movie

@Database(entities = [Movie::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun movieDAO(): MovieDAO
}