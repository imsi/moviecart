package com.ingridsantos.moviecart.di

import androidx.room.Room
import com.ingridsantos.moviecart.model.repository.MovieRepository
import com.ingridsantos.moviecart.model.repository.MovieRepositoryImp
import com.ingridsantos.moviecart.model.repository.database.Database
import com.ingridsantos.moviecart.model.repository.network.MoviesAPI
import com.ingridsantos.moviecart.model.repository.network.RetrofitClient
import com.ingridsantos.moviecart.viewmodel.DetailMovieViewModel
import com.ingridsantos.moviecart.viewmodel.ListMoviesViewModel
import com.ingridsantos.moviecart.viewmodel.MoviesCartViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module {
    single { Room.databaseBuilder(get(), Database::class.java, "database_movies").build() }
    single { get<Database>().movieDAO() }
    single { RetrofitClient.getInstancerRetrofit().create(MoviesAPI::class.java) }
    factory<MovieRepository> { MovieRepositoryImp(get(), get()) }
    viewModel { ListMoviesViewModel(get()) }
    viewModel { DetailMovieViewModel(get()) }
    viewModel { MoviesCartViewModel(get()) }
}